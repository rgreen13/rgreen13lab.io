<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Robert C. Green II, Ph.D.</title>
    <description></description>
    <link>http://rgreen13.gitlab.io/</link>
    <atom:link href="http://rgreen13.gitlab.io/feed.xml" rel="self" type="application/rss+xml"/>
    <pubDate>Tue, 02 Mar 2021 19:56:29 -0500</pubDate>
    <lastBuildDate>Tue, 02 Mar 2021 19:56:29 -0500</lastBuildDate>
    <generator>Jekyll v3.8.5</generator>
    
      <item>
        <title>Making a Career Move to Computer Science and Software Engineering</title>
        <description>&lt;p&gt;At my day job, I’m currently the Graduate Coordinator for &lt;a href=&quot;https://www.bgsu.edu&quot;&gt;Computer Science (CS)&lt;/a&gt; at &lt;a href=&quot;https://www.bgsu.edu&quot;&gt;Bowling Green State University (BGSU)&lt;/a&gt;. As part of that program, we offer an &lt;a href=&quot;https://www.bgsu.edu/arts-and-sciences/computer-science/undergraduate/bachelor-of-science-se-degree.html&quot;&gt;Undergraduate Degree in Software Engineering (SE)&lt;/a&gt;, a &lt;a href=&quot;https://www.bgsu.edu/arts-and-sciences/computer-science/graduate/ms-se-certificate.html&quot;&gt;Graduate Certificate in SE&lt;/a&gt;, and a &lt;a href=&quot;https://www.bgsu.edu/arts-and-sciences/computer-science/graduate/ms-specializations.html&quot;&gt;MS in CS with a Specialization in SE&lt;/a&gt;. Because of this, I get a lot of questions asking me about changing careers to CS and SE, with most inquiries coming from folks who have currently earned a degree in a non-CS major. Many also have advanced degrees, most frequently a MBA. The questions are generally the same:&lt;/p&gt;

&lt;ol&gt;
  &lt;li&gt;How can I make a career move to CS or SE?&lt;/li&gt;
  &lt;li&gt;Should I get a second undergraduate degree, complete a certificate, or pursue a MS?&lt;/li&gt;
  &lt;li&gt;How long will it take me?&lt;/li&gt;
&lt;/ol&gt;

&lt;h2 id=&quot;what-advice-do-i-give-about-this&quot;&gt;What advice do I give about this?&lt;/h2&gt;

&lt;ul&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Start building a portfolio.&lt;/strong&gt; No matter what you do, start building a portfolio on &lt;a href=&quot;https://gitlab.com&quot;&gt;Gitlab&lt;/a&gt; or &lt;a href=&quot;https://github.com&quot;&gt;Github&lt;/a&gt;). Personally I prefer &lt;a href=&quot;https://gitlab.com&quot;&gt;Gitlab&lt;/a&gt;, but the choice is up to you. Build example projects, run through tutorials, and demonstrate that you can not only develop software but follow Software Engineering practices. Demonstrate your ability to branch and merge, to collaborate with others, and to leverage the DevOps tools like those available via &lt;a href=&quot;https://gitlab.com&quot;&gt;Gitlab&lt;/a&gt;. In other words, show the world what you are capable of!  One word of caution - don’t post or celebrate bad work. Keep repos of your work (and assignments) private until you are ready for someone to see them.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Consider attending tech meetups in your area.&lt;/strong&gt; It’s never to early to start building your network. You need to meet other CS and SE folks for guidance, mentoring, and the sharing of skills. And you’ll make some new friends along the way! For instance, if you are in the Toledo area, checkout &lt;a href=&quot;https://www.meetup.com/cities/us/oh/toledo/tech/&quot;&gt;Toledo Tech Meetups&lt;/a&gt; and &lt;a href=&quot;https://www.meetup.com/Toledo-Web-Professionals/&quot;&gt;Toledo Web Professionals&lt;/a&gt;. You may also want to join professional societies with local chapters like &lt;a href=&quot;https://www.acm.org&quot;&gt;ACM&lt;/a&gt; or &lt;a href=&quot;https://www.ieee.org&quot;&gt;IEEE&lt;/a&gt;. Whatever you do, make some connections! They may help you in the future, and you may be able to use them to connect others later on.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;A second undergraduate degree takes more time, though it is more thorough.&lt;/strong&gt; For instance, at &lt;a href=&quot;https://www.bgsu.edu&quot;&gt;BGSU&lt;/a&gt; the &lt;a href=&quot;https://www.bgsu.edu/content/dam/BGSU/catalog/Fall-2019/arts-and-sciences/Software-Engineering.pdf&quot;&gt;core courses&lt;/a&gt; alone for the BS in SE count for a total of 52 credit hours. At a full time load of 16 credit hours, that’s a bit over three academic semesters, which is far too long for most professionals. Would it be worth it? Absolutely! Getting that complete education would make you a potentially excellent SE professional, but normally the time/cost trade-off doesn’t work out for those who already hold degrees or are working full time.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;A SE certificate or micro-course is a good start towards an official credential.&lt;/strong&gt; Courses in a certificate typically mimic those in a specialization or SE degree. This means that by completing a certificate you are well on your way towards becoming a SE or joining a MS program. Many certificate programs also offer special value adds such as guaranteed admission to the MS Degree after completion or something similar (this is the case at BGSU). With many certificates being roughly 12 credit hours, your almost halfway to a degree anyway (at least in Ohio)!&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Look for transitional courses.&lt;/strong&gt; While many programs assume an academic or professional background in CS, SE, or a related field, many programs also offer transitional courses. For instance, in CS at BGSU we offer &lt;a href=&quot;https://www.bgsu.edu/content/dam/BGSU/college-of-arts-and-sciences/computer-science/documents/syllabi/cs5010.pdf&quot;&gt;CS 5010: Fundamentals of Programming&lt;/a&gt; and &lt;a href=&quot;https://www.bgsu.edu/content/dam/BGSU/college-of-arts-and-sciences/computer-science/documents/syllabi/cs5010.pdf&quot;&gt;CS 5020: Fundamentals of Computer Science&lt;/a&gt; as online courses during the summer. The courses don’t count toward the degree, but will give you the fundamentals you need to start a transition,&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;The MS in SE or a MS in CS with a SE specialization is a much stronger credential.&lt;/strong&gt; Why would that be? The program is longer, requires more development of mathematical and SE skills, and requires the ability to complete some type of practical or research-based project resulting in reports, presentation, and, generally, working software. This means that your technical depth/breadth will increase and you will also spend time focused on growing problem solving, research, and communication skills. Many potential students don’t consider this, but one of the greatest value adds of graduate level education in STEM is improving these non-technical skills. In addition, many employers expect that those with advanced credentials will move up the corporate ladder and take on responsibility more quickly than those without these skills.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Look for a program that meets your needs in terms of offerings.&lt;/strong&gt; Regardless of the program you choose, make sure that it fits you. Online, evening, and full-time classes should all be options with many being offered in multiple formats over multiple semesters (Fall, Spring, Summer) in multiple formats (6 weeks, 7 weeks, 12 weeks, or 15 weeks for example). If you like more personalized support, choose a regional College or University that you can visit or, at the very least, choose a program in a similar time zone so that hours of access to instructors are available&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Look for a program with strong industrial ties.&lt;/strong&gt; Your end goal is a new and better job/career that will improve your life and the lives of those you love, so make sure you’re in the right place to get one. Most CS and SE departments have industrial advisory boards, so find out who the members are. Ask where the graduates find jobs. Ask what percentage of the graduates find jobs. Do graduates typically find jobs regionally or nationally and which do you prefer? For example, graduates from the CS program at BGSU tend to be hired by regional employers like Progressive, Nationwide, Hyland, Medical Mutal, Marathon, Honda, HMB, Accenture, and many others.&lt;/p&gt;
  &lt;/li&gt;
&lt;/ul&gt;
</description>
        <pubDate>Tue, 28 Jan 2020 00:00:00 -0500</pubDate>
        <link>http://rgreen13.gitlab.io/blog/2020/01/making-a-career-move-to-cs-and-se</link>
        <guid isPermaLink="true">http://rgreen13.gitlab.io/blog/2020/01/making-a-career-move-to-cs-and-se</guid>
        
        
        <category>bgsu</category>
        
        <category>software engineering</category>
        
        <category>education</category>
        
      </item>
    
      <item>
        <title>Jekyll: Docker to the Rescue</title>
        <description>&lt;p&gt;This weekend I thought I would be clever and migrate my website to a new template. So, I chose &lt;a href=&quot;https://github.com/alshedivat/al-folio&quot;&gt;al-folio&lt;/a&gt; and started down this path. I cloned the repo into a new folder, and issued the common command &lt;code class=&quot;highlighter-rouge&quot;&gt;bundle install&lt;/code&gt;. And while I expected all things to roll smoothly, everything thought otherwise.&lt;/p&gt;

&lt;ul&gt;
  &lt;li&gt;Ruby &amp;gt;= 2.4.4 is required though Mac OS only has 2.3.7. Enter &lt;code class=&quot;highlighter-rouge&quot;&gt;rubyenv&lt;/code&gt;.&lt;/li&gt;
  &lt;li&gt;&lt;code class=&quot;highlighter-rouge&quot;&gt;rbenv install 2.6.3&lt;/code&gt; . That results in multiple errors related to &lt;a href=&quot;https://github.com/libffi/libffi&quot;&gt;&lt;code class=&quot;highlighter-rouge&quot;&gt;FFI&lt;/code&gt;&lt;/a&gt;. Just one of the errors looked like this:&lt;/li&gt;
&lt;/ul&gt;

&lt;div class=&quot;highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt;Last 10 log lines:
compile.c:1233:1: warning: 'rb_iseq_t* new_child_iseq_ifunc(rb_iseq_t*, const vm_ifunc*, VALUE, const rb_iseq_t*, int, int)' defined but not used [-Wunused-function]
 new_child_iseq_ifunc(rb_iseq_t *iseq, const struct vm_ifunc *ifunc,
 ^~~~~~~~~~~~~~~~~~~~
compile.c:1213:1: warning: 'rb_iseq_t* new_child_iseq(rb_iseq_t*, const NODE*, VALUE, const rb_iseq_t*, int, int)' defined but not used [-Wunused-function]
 new_child_iseq(rb_iseq_t *iseq, const NODE *const node,
 ^~~~~~~~~~~~~~
cc1plus: warning: unrecognized command line option '-Wno-self-assign'
cc1plus: warning: unrecognized command line option '-Wno-parentheses-equality'
cc1plus: warning: unrecognized command line option '-Wno-constant-logical-operand'
make: *** [compile.o] Error 1
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;/div&gt;

&lt;ul&gt;
  &lt;li&gt;Seems it had something to do with &lt;code class=&quot;highlighter-rouge&quot;&gt;clang&lt;/code&gt; vs &lt;code class=&quot;highlighter-rouge&quot;&gt;gcc&lt;/code&gt;, so I ran the following:
    &lt;ul&gt;
      &lt;li&gt;&lt;code class=&quot;highlighter-rouge&quot;&gt;export CC=gcc-8&lt;/code&gt;&lt;/li&gt;
      &lt;li&gt;&lt;code class=&quot;highlighter-rouge&quot;&gt;export CXX=g++-8&lt;/code&gt;&lt;/li&gt;
    &lt;/ul&gt;
  &lt;/li&gt;
  &lt;li&gt;That didn’t work either. I result the variables.&lt;/li&gt;
  &lt;li&gt;Searched around for more solutions. Ended up running &lt;code class=&quot;highlighter-rouge&quot;&gt;brew upgrade&lt;/code&gt;. That was nice, but it also broke &lt;code class=&quot;highlighter-rouge&quot;&gt;pyenv&lt;/code&gt; (fixed with a simple &lt;code class=&quot;highlighter-rouge&quot;&gt;pyenv rhash&lt;/code&gt;)&lt;/li&gt;
  &lt;li&gt;Things still didn’t work, so I figured Docker would save me. And it did!
    &lt;ul&gt;
      &lt;li&gt;&lt;code class=&quot;highlighter-rouge&quot;&gt;docker run --rm --label=jekyll --volume=&quot;$PWD&quot;:/srv/jekyll  -it -p 4000:4000 jekyll/jekyll jekyll serve&lt;/code&gt;&lt;/li&gt;
    &lt;/ul&gt;
  &lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;And suddenly everything was beautiful! So, I could have wasted my time. I could have learned something. I could have solved the problem and walked away knowing some inside secret. Instead, I chose to use my time wisely and move to Docker. One quick search, one line of code, and my world was repaired. Thank you &lt;a href=&quot;https://www.docker.com&quot;&gt;Docker&lt;/a&gt;!&lt;/p&gt;

&lt;p&gt;Only thing was, after a restart, suddenly &lt;code class=&quot;highlighter-rouge&quot;&gt;rbenv&lt;/code&gt; was happy to install Ruby and the &lt;code class=&quot;highlighter-rouge&quot;&gt;bundle install&lt;/code&gt; command worked flawlessly. Now everything works. &lt;strong&gt;But if I would have started with Docker, I would have saved an entire day!&lt;/strong&gt;&lt;/p&gt;

</description>
        <pubDate>Mon, 16 Sep 2019 00:00:00 -0400</pubDate>
        <link>http://rgreen13.gitlab.io/blog/2019/09/jekyll-docker-to-the-rescue</link>
        <guid isPermaLink="true">http://rgreen13.gitlab.io/blog/2019/09/jekyll-docker-to-the-rescue</guid>
        
        
        <category>jekyll</category>
        
        <category>docker</category>
        
        <category>mac</category>
        
      </item>
    
      <item>
        <title>Setting up Ubuntu 18.04 for an Existing Laravel Project with Sqlite</title>
        <description>&lt;p&gt;Many of the projects in the Software Engineering Project Course I teach (CS 4540/5540) at Bowling Green State University (BGSU) requires students to continue work on existing or incomplete projects. Many of these projects are built using the &lt;a href=&quot;https://laravel.com/&quot;&gt;Laravel&lt;/a&gt; framework in PHP. This guide is intended to explain how to setup an Ubuntu 18.XX server to host these projects. This guide is heavily based on the work at &lt;a href=&quot;https://www.howtoforge.com/tutorial/install-laravel-on-ubuntu-for-apache/&quot;&gt;HowToForge&lt;/a&gt;.&lt;/p&gt;

&lt;ol&gt;
  &lt;li&gt;
    &lt;p&gt;When starting off any software installation on a Debian-based Linux distribution, one should always being by updating the system using the following commands. For any readers that are unfamiliar, the various pieces of this command are defined as follows:&lt;/p&gt;

    &lt;ul&gt;
      &lt;li&gt;&lt;code class=&quot;highlighter-rouge&quot;&gt;sudo&lt;/code&gt; is a command that is short for &quot;Super User Do&quot;. Using this command, if you have permissions, enables you to run commands as if you were the system super user.&lt;/li&gt;
      &lt;li&gt;&lt;code class=&quot;highlighter-rouge&quot;&gt;apt&lt;/code&gt; is a package manager that is commonly used on Debian-based Linux Distributions. &lt;code class=&quot;highlighter-rouge&quot;&gt;apt-get&lt;/code&gt; is a command commonly used to retrieve software packages for the system (hence the &quot;get&quot;).&lt;/li&gt;
    &lt;/ul&gt;

    &lt;div class=&quot;language-bash highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt; &lt;span class=&quot;nb&quot;&gt;sudo &lt;/span&gt;apt-get update 
 &lt;span class=&quot;nb&quot;&gt;sudo &lt;/span&gt;apt-get upgrade
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;After updating the system, it’s now time to install the latest versions of Apache Web Server, the PHP programming language, and the Composer utility. Many guides you will find elsewhere will give you different instructions to install Composer, but the software is currently a package contained within the software repository listed below, making it much easier to install.&lt;/p&gt;

    &lt;div class=&quot;language-bash highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt; &lt;span class=&quot;nb&quot;&gt;sudo &lt;/span&gt;add-apt-repository ppa:ondrej/php
 &lt;span class=&quot;nb&quot;&gt;sudo &lt;/span&gt;apt-get update
 &lt;span class=&quot;nb&quot;&gt;sudo &lt;/span&gt;apt-get &lt;span class=&quot;nb&quot;&gt;install &lt;/span&gt;apache2 libapache2-mod-php7.2 php7.2 php7.2-xml php7.2-gd php7.2-opcache php7.2-mbstring php7.2-sqlite3 composer
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;With the required software installed, it’s necessary to make sure that the PHP 7.0 module (or whatever other version of PHP that was previously installed) is removed. The quickest way to make sure you are removing the proper version is to 1) After entering &lt;code class=&quot;highlighter-rouge&quot;&gt;sudo a2dismod php&lt;/code&gt; press the &lt;code class=&quot;highlighter-rouge&quot;&gt;tab&lt;/code&gt; key which will autocomplete, providing a list of items to remove. The first command below removes the module from Apache while the second line adds the necessary module back into Apache.&lt;/p&gt;

    &lt;div class=&quot;language-bash highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt; &lt;span class=&quot;nb&quot;&gt;sudo &lt;/span&gt;a2dismod php7.0
 &lt;span class=&quot;nb&quot;&gt;sudo &lt;/span&gt;a2enmod php7.2
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;Once complete, change to the default hosting directory &lt;code class=&quot;highlighter-rouge&quot;&gt;/var/www/html&lt;/code&gt; and clone your existing project using &lt;code class=&quot;highlighter-rouge&quot;&gt;git&lt;/code&gt;. If for some reason &lt;code class=&quot;highlighter-rouge&quot;&gt;git&lt;/code&gt; was not installed, it’s as close as a &lt;code class=&quot;highlighter-rouge&quot;&gt;sudo apt-get install git&lt;/code&gt;. &lt;strong&gt;Make sure to clone using the &lt;code class=&quot;highlighter-rouge&quot;&gt;https&lt;/code&gt; option unless you are comfortable setting up your SSH keys.&lt;/strong&gt;&lt;/p&gt;

    &lt;div class=&quot;language-bash highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt; &lt;span class=&quot;nb&quot;&gt;cd&lt;/span&gt; /var/www/html 
 &lt;span class=&quot;nb&quot;&gt;sudo &lt;/span&gt;git clone &lt;span class=&quot;o&quot;&gt;[&lt;/span&gt;PROJECT_REPOSITORY]
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;Change into the newly created directory. Make sure you are in the root of the Laravel project (for some projects, you may have to go one level deeper). You will know  you are in the proper directory when you directories like &lt;code class=&quot;highlighter-rouge&quot;&gt;app&lt;/code&gt;  and &lt;code class=&quot;highlighter-rouge&quot;&gt;public&lt;/code&gt;. Once there, use composer to install the necessary packages.&lt;/p&gt;

    &lt;div class=&quot;language-bash highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt; &lt;span class=&quot;nb&quot;&gt;cd&lt;/span&gt; &lt;span class=&quot;o&quot;&gt;[&lt;/span&gt;NEW_DIRECTORY]
 &lt;span class=&quot;nb&quot;&gt;sudo &lt;/span&gt;composer &lt;span class=&quot;nb&quot;&gt;install&lt;/span&gt;
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;Setting up the Sqlite database begins with the creation of a single file. Make sure to use &lt;code class=&quot;highlighter-rouge&quot;&gt;sudo&lt;/code&gt; in front of this command as you will likely not have permissions to create the file without it.&lt;/p&gt;

    &lt;div class=&quot;language-bash highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt; &lt;span class=&quot;nb&quot;&gt;sudo touch &lt;/span&gt;database/database.sqlite
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;With the file created, edit the &lt;code class=&quot;highlighter-rouge&quot;&gt;.env&lt;/code&gt; file to setup proper database access. Typically this file will not exist at all, so you’ll first have to copy the &lt;code class=&quot;highlighter-rouge&quot;&gt;.env-example&lt;/code&gt; file to &lt;code class=&quot;highlighter-rouge&quot;&gt;.env&lt;/code&gt; as shown below.&lt;/p&gt;

    &lt;div class=&quot;highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt; sudo cp .env-example .env
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;Once the &lt;code class=&quot;highlighter-rouge&quot;&gt;.env&lt;/code&gt; file is created, modify the database connection section so that it is properly set for sqlite. The details are shown below. Do make sure that the project directory is set correctly.&lt;/p&gt;

    &lt;div class=&quot;highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt; DB_CONNECTION=sqlite
 DB_DATABASE=/var/www/html/[PROJECT_DIRECTORY]/database/database.sqlite
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;With the database file and connection in place, all migrations and seeds can be run to 1) Create database structure and 2) Seed the database with initial data. More details on both can be found in the Laravel docs. In addition, make sure to generate the encryption key for the application or you will run into errors.&lt;/p&gt;

    &lt;div class=&quot;highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt; sudo php artisan migrate
 sudo php artisan db:seed
 sudo php artisan key:generate
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;With Laraval all set, Apache now needs configured for proper hosting. This begins by changing the group ownership and the permissions to certain directories within your project. This can be done with the following two lines:&lt;/p&gt;

    &lt;div class=&quot;language-bash highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt;&lt;span class=&quot;nb&quot;&gt;sudo chgrp&lt;/span&gt; &lt;span class=&quot;nt&quot;&gt;-R&lt;/span&gt; www-data /var/www/html/your-project
&lt;span class=&quot;nb&quot;&gt;sudo chmod&lt;/span&gt; &lt;span class=&quot;nt&quot;&gt;-R&lt;/span&gt; 775 /var/www/html/your-project/storage
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;With correct ile permissions in place, you now have to create a configuration fiile to tell Apache where to find your project.  This is accomplished by creating a new file in the &lt;code class=&quot;highlighter-rouge&quot;&gt;/etc/apache2/sites-available&lt;/code&gt; directory. This directory contains multiple configurations for various websites that are available on your server. While &lt;code class=&quot;highlighter-rouge&quot;&gt;nano&lt;/code&gt; is used to edit in this example, any editor can be used.&lt;/p&gt;

    &lt;div class=&quot;language-bash highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt;&lt;span class=&quot;nb&quot;&gt;cd&lt;/span&gt; /etc/apache2/sites-available
&lt;span class=&quot;nb&quot;&gt;sudo &lt;/span&gt;nano laravel.conf
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;Enter the following code into your &lt;code class=&quot;highlighter-rouge&quot;&gt;laravel.conf&lt;/code&gt; file. This tells Apache where your files are, the URL of your website, and a few other key items that allow Laravel to work.&lt;/p&gt;

    &lt;div class=&quot;language-bash highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt;&amp;lt;VirtualHost &lt;span class=&quot;k&quot;&gt;*&lt;/span&gt;:80&amp;gt;
    ServerName URL_OF_VM

    ServerAdmin YourEmail@YourDomain.com
    DocumentRoot /var/www/html/[PROJECT_DIRECTORY]/public

    &amp;lt;Directory /var/www/html/PROJECT_DIRECTORY&amp;gt;
        AllowOverride All
    &amp;lt;/Directory&amp;gt;

    ErrorLog &lt;span class=&quot;k&quot;&gt;${&lt;/span&gt;&lt;span class=&quot;nv&quot;&gt;APACHE_LOG_DIR&lt;/span&gt;&lt;span class=&quot;k&quot;&gt;}&lt;/span&gt;/error.log
    CustomLog &lt;span class=&quot;k&quot;&gt;${&lt;/span&gt;&lt;span class=&quot;nv&quot;&gt;APACHE_LOG_DIR&lt;/span&gt;&lt;span class=&quot;k&quot;&gt;}&lt;/span&gt;/access.log combined
&amp;lt;/VirtualHost&amp;gt;
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;Save your &lt;code class=&quot;highlighter-rouge&quot;&gt;laravel.conf&lt;/code&gt; file.&lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;The final step involves removing the default website (&lt;code class=&quot;highlighter-rouge&quot;&gt;a2dissite&lt;/code&gt;), adding your newly created site (&lt;code class=&quot;highlighter-rouge&quot;&gt;a2ensite&lt;/code&gt;), enabling the rewrite module to use clean URLs (&lt;code class=&quot;highlighter-rouge&quot;&gt;a2enmod&lt;/code&gt;), and the restarting the Apache service to pick up all changes.&lt;/p&gt;

    &lt;div class=&quot;highlighter-rouge&quot;&gt;&lt;div class=&quot;highlight&quot;&gt;&lt;pre class=&quot;highlight&quot;&gt;&lt;code&gt;sudo a2dissite 000-default.conf
sudo a2ensite laravel.conf
sudo a2enmod rewrite
sudo service apache2 restart
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li&gt;Done!&lt;/li&gt;
&lt;/ol&gt;
</description>
        <pubDate>Fri, 22 Feb 2019 00:00:00 -0500</pubDate>
        <link>http://rgreen13.gitlab.io/blog/2019/02/installing-laravel-with-sqlite-existing-project</link>
        <guid isPermaLink="true">http://rgreen13.gitlab.io/blog/2019/02/installing-laravel-with-sqlite-existing-project</guid>
        
        
        <category>software engineering</category>
        
        <category>teaching</category>
        
        <category>guides</category>
        
      </item>
    
      <item>
        <title>Projects from CS 4540/5540: Software Development Project</title>
        <description>&lt;p&gt;During the Fall of 2016, I had the pleasure of teaching CS 4540/5540: Software Development Project at BGSU. This class, fundamentally, teaches students how to build real software for real clients using real software development tools and the Agile method. This year all code was hosted on Gitlab, students communicated via Slack, all web-based projects were built using Laravel, and all games were built using Unity3D.&lt;/p&gt;

&lt;p&gt;Over the semester, two sections of this course set off to build a dozen different projects. Of special note was our partnership with Digital Arts via ARTC 4420: Art and Virtual Environment to build a variety of games in conjunction with their students. In fact, one of those games, Paper Skies, is in the Android App Store! Check it out &lt;a href=&quot;https://play.google.com/store/apps/details?id=com.BowlingGreenStateUniversitycs4540.PaperSkies&quot;&gt;here&lt;/a&gt;.&lt;/p&gt;

&lt;p&gt;Some of the other projects that were developed include:&lt;/p&gt;
&lt;ol&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;CS Welcome Display:&lt;/strong&gt; A web-based prototype for a touch-screen based information kiosk in the Department of Computer Science at BGSU.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;EASEL:&lt;/strong&gt; The Education through Application-Supported Experiential Learning (EASEL) system is a new type of learning system which considers the student at the core of
the learning environment. It draws upon basic theory of constructivism (Piaget) and (Kolb). Many learning environments provide an opportunity for students to reflect on their work, which is a crucial part of experiential learning theory. A student may be asked to write a journal entry or take a post-survey based on a specific experience. But often, this type of data is collected a while after the experience has taken place. This system allows the student to collect the data in the right place at the right time while sharing it with their instructor.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;CMA Inventory System:&lt;/strong&gt; The College of Musical Arts (CMA) has long needed an inventory system in order to manage their collection of instruments. This web-based project allows the CMA to efficiently track all of their instruments.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Our Food Farm&lt;/strong&gt;: A web-based application that allows local growers and sellers to connect via virtual food stands to buy, sell, and trade products.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;oneList:&lt;/strong&gt; A spiced up task list that allows for some unique features including specialty filtering and the integration of hierarchal Kanban boards&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Life Tracker:&lt;/strong&gt; A research oriented project for investigating various aspects of the lives of underprivileged individuals.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;AVVRE:&lt;/strong&gt; Explore a beautifully crafted 3D world in an effort to find out where you are and why you’re there.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Come Back Home:&lt;/strong&gt; The village is gone, and all that remains is a single young girl. Can she figure out what happened to her people?&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;ENVIRO:&lt;/strong&gt; The world is a dirty place. Let’s start cleaning it up one piece of trash at a time.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Keep Running:&lt;/strong&gt; Keep moving or the Zombies will get you.&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Stella:&lt;/strong&gt; Bulid your own planet. Manage the resources. Watch the population grow!&lt;/p&gt;
  &lt;/li&gt;
  &lt;li&gt;
    &lt;p&gt;&lt;strong&gt;Paper Skies:&lt;/strong&gt; Fly a paper airplane through multiple levels of challenges as you’re attacked by crazy birds. Available on the &lt;a href=&quot;https://play.google.com/store/apps/details?id=com.BowlingGreenStateUniversitycs4540.PaperSkies&quot;&gt;Play Store&lt;/a&gt;.&lt;/p&gt;
  &lt;/li&gt;
&lt;/ol&gt;

</description>
        <pubDate>Fri, 16 Dec 2016 00:00:00 -0500</pubDate>
        <link>http://rgreen13.gitlab.io/blog/2016/12/projects-from-cs-4540-5540</link>
        <guid isPermaLink="true">http://rgreen13.gitlab.io/blog/2016/12/projects-from-cs-4540-5540</guid>
        
        
        <category>software engineering</category>
        
        <category>teaching</category>
        
      </item>
    
      <item>
        <title>Industrial-Scale Agile: From Craft to Engineering</title>
        <description>&lt;p&gt;A very interesting &lt;a href=&quot;http://cacm.acm.org/magazines/2016/12/210383-industrial-scale-agile/abstract&quot;&gt;article&lt;/a&gt; just came out in the Communications of the ACM, and last night my students in CS 4540/5540 had a great discussion considering the age old question, ‘Is Software Engineering really engineering?’. My favorite portion of the article juxtaposes the concepts of a Craft with Engineering. Really beautiful work.&lt;/p&gt;

&lt;p&gt;Yet, while this argument has been going on for ages, I’d also argue that there’s is currently a split going on in the discipline of Software Engineering (SE) - I can see it and a I can feel it. I was speaking with multiple students after class, and I likened it to the choice between Electrician and Electrical Engineer - One is a trade and the other is an engineering discipline. I like how this post at [Our Everyday Life][http://oureverydaylife.com/difference-between-electrician-electrical-engineer-12215.html] puts it:&lt;/p&gt;

&lt;blockquote&gt;
  &lt;p&gt;Electrical engineers design the power systems and the equipment that distribute energy. Electricians install wiring and make electrical repairs. They each have differing responsibilities, qualifications and job opportunities.&lt;/p&gt;
&lt;/blockquote&gt;

&lt;p&gt;I can see this in a few years in SE:&lt;/p&gt;

&lt;blockquote&gt;
  &lt;p&gt;Software engineers design the software architecture and systems. Developers write program code, perform bug fixes, and do testing. They each have differing responsibilities, qualifications and job opportunities.&lt;/p&gt;
&lt;/blockquote&gt;

&lt;p&gt;And the comparisons can go on, whether in type of education/training, etc. It feels like like SE is growing up.&lt;/p&gt;

</description>
        <pubDate>Tue, 06 Dec 2016 00:00:00 -0500</pubDate>
        <link>http://rgreen13.gitlab.io/blog/2016/12/se-craft-to-engineering</link>
        <guid isPermaLink="true">http://rgreen13.gitlab.io/blog/2016/12/se-craft-to-engineering</guid>
        
        
        <category>software engineering</category>
        
        <category>agile</category>
        
      </item>
    
  </channel>
</rss>
