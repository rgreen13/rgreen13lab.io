---
layout: page
title: "MBA 6010 - Robert C. Green II, Ph.D."
date: 2015-06-30 14:02
comments: false
sharing: false
footer: false
---

<div class="row-fluid">
    <div class="span6">
        <h4>MBA 6010 - Quantitative Analysis for Managers</h4>
        <table class="table table-condensed table-bordered">
            <tbody>
                <tr>
                    <th>Number</th>
                    <th colspan="4" style="text-align:center;">Lecture</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture01_Intro.html">Introduction</a></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture02_Modeling.html">Modeling</a></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture03_LinearPrograms.html">Formulating Linear Programs</a></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture04_LP_In_Excel.html">Linear Programs in Excel</a></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture05_MakeBuy_Diet.html">Make vs. Buy and Diet Problems</a></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture06_Blending.html">Blending Problems</a></td>
                </tr>
                 <tr>
                    <td>7</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture07_Financial.html">Financial Problems</a></td>
                </tr>
                 <tr>
                    <td>8</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture08_MultiPeriod.html">MultiPeriod Problems</a></td>
                </tr>
                <tr>
                    <td>9</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture09_Transportation.html">Transportation Problems</a></td>
                </tr>
                <tr>
                    <td>10</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture10_Sensitivity_Analysis.html">Sensitivity Analysis</a></td>
                </tr>
                <tr>
                    <td>11</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture11_Network_Modeling.html">Network Modeling</a></td>
                </tr>
                <tr>
                    <td>12</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture12_Integer_Programming.html">Integer Linear Programming</a></td>
                </tr>
                <tr>
                    <td>13</td>
                    <td><a target="_blank" href="/assets/mba6010/Lecture13_Simulation.html">Simulation</a></td>
                </tr>

               
            </tbody>
        </table>
    </div>
</div>