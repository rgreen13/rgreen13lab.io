---
layout: page
title: "CS 3060 - Programming Languages"
date: 2017-08-23 16:52
comments: false
sharing: false
footer: true
---

<div class="row-fluid">
    <div class="span6">
        <table class="table table-condensed table-bordered">
            <tbody>
                <tr>
                    <th>Language</th>
                    <th colspan="4" style="text-align:center;">Lectures</th>
                </tr>
                <tr>
                    <th>Introduction</th>
                    <td colspan="4" style="text-align:center;">
                        <a target="_blank" href="lectures/Lecture1-Intro.html">Day 1</a>
                    </td>
                </tr>

                <tr>
                    <th>Ruby</th>
                    <td>
                        <a target="_blank" href="lectures/Lecture2-Ruby-Day1.html">Day 1</a>
                        <a target="_blank" href="lectures/Lecture2-Ruby-Day1.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture3-Ruby-Day2.html">Day 2</a>
                        <a target="_blank" href="lectures/Lecture3-Ruby-Day2.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture4-Ruby-Day3.html">Day 3</a>
                        <a target="_blank" href="lectures/Lecture4-Ruby-Day3.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture5-Ruby-Day4.html">Day 4</a>
                        <a target="_blank" href="lectures/Lecture5-Ruby-Day4.html?print-pdf">(PDF)</a>
                    </td>
                </tr>

                <tr>
                    <th>Io</th>
                    <td>
                        <a target="_blank" href="lectures/Lecture6-Io-Day1.html">Day 1</a>
                        <a target="_blank" href="lectures/Lecture6-Io-Day1.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture7-Io-Day2.html">Day 2</a>
                        <a target="_blank" href="lectures/Lecture7-Io-Day2.html?print-pdf">(PDF)</a>
                    </td>
                    
                    <td>
                        <a target="_blank" href="lectures/Lecture8-Io-Day3.html">Day 3</a>
                        <a target="_blank" href="lectures/Lecture8-Io-Day3.html?print-pdf">(PDF)</a>
                    </td>
                </tr>
                
                <tr>
                    <th>Scala</th>
                    <td>
                        <a target="_blank" href="lectures/Lecture9-Scala-Day1.html">Day 1</a>
                        <a target="_blank" href="lectures/Lecture9-Scala-Day1.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture10-Scala-Day2.html">Day 2</a>
                        <a target="_blank" href="lectures/Lecture10-Scala-Day2.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture11-Scala-Day3.html">Day 3</a>
                        <a target="_blank" href="lectures/Lecture11-Scala-Day3.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture12-Scala-Day4.html">Day 4</a>
                        <a target="_blank" href="lectures/Lecture12-Scala-Day4.html?print-pdf">(PDF)</a>
                    </td>
                </tr>
                
                <tr>
                    <th>Erlang</th>
                    <td><a target="_blank" href="lectures/Lecture13-Erlang-Day1.html">Day 1</a>
                        <a target="_blank" href="lectures/Lecture13-Erlang-Day1.html?print-pdf">(PDF)</a>
                    </td>
                    <td><a target="_blank" href="lectures/Lecture14-Erlang-Day2.html">Day 2</a>
                        <a target="_blank" href="lectures/Lecture14-Erlang-Day2.html?print-pdf">(PDF)</a>
                    </td>
                    <td><a target="_blank" href="lectures/Lecture15-Erlang-Day3.html">Day 3</a>
                        <a target="_blank" href="lectures/Lecture15-Erlang-Day3.html?print-pdf">(PDF)</a>
                    </td>
                    <td><a target="_blank" href="lectures/Lecture16-Erlang-Day4.html">Day 4</a>
                        <a target="_blank" href="lectures/Lecture16-Erlang-Day4.html?print-pdf">(PDF)</a>
                    </td>
                </tr>
                <tr>
                    <th>Prolog</th>
                    <td>
                        <a target="_blank" href="lectures/Lecture17-Prolog-Day1.html">Day 1</a>
                        <a target="_blank" href="lectures/Lecture17-Prolog-Day1.html?print-pdf">(PDF)</a>
                    </td>
                    <td>    
                        <a target="_blank" href="lectures/Lecture18-Prolog-Day2.html">Day 2</a>
                        <a target="_blank" href="lectures/Lecture18-Prolog-Day2.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture19-Prolog-Day3.html">Day 3</a>
                        <a target="_blank" href="lectures/Lecture19-Prolog-Day3.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture20-Prolog-Day4.html">Day 4</a>
                        <a target="_blank" href="lectures/Lecture20-Prolog-Day4.html?print-pdf">(PDF)</a>
                    </td>
                </tr>
                
                <tr>
                    <th>Lua</th>
                    <td>
                        <a target="_blank" href="lectures/Lecture24-Lua-Day1.html">Day 1</a>
                        <a target="_blank" href="lectures/Lecture24-Lua-Day1.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture25-Lua-Day2.html">Day 2</a>
                        <a target="_blank" href="lectures/Lecture25-Lua-Day2.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture26-Lua-Day3.html">Day 3</a>
                        <a target="_blank" href="lectures/Lecture26-Lua-Day3.html?print-pdf">(PDF)</a>
                    </td>
                    <td>
                        <a target="_blank" href="lectures/Lecture27-Lua-Day4.html">Day 4</a>
                        <a target="_blank" href="lectures/Lecture27-Lua-Day4.html?print-pdf">(PDF)</a>
                    </td>
                </tr> 
            </tbody>
        </table>
    </div>
</div>
