---
layout: page
title: "Projects"
comments: false
sharing: false
footer: false
permalink: /projects/
---

<style>
    li{
        padding-bottom:5px;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-12" style="margin-bottom:10px;">
            <p>
                All of my projects involve aspects of <a href="https://en.wikipedia.org/wiki/Computational_intelligence">Computational Intelligence (Neural Networks, Evolutionary Computation, and Swarm Computation)</a>, <a href="https://en.wikipedia.org/wiki/Parallel_computing">High Performance/Parallel Computing</a>, and <a href="https://en.wikipedia.org/wiki/Data_science">Data Science</a>. In addition, I am interested in working with multiple datasets including the <a href="https://archive.ics.uci.edu/ml/datasets/forest+fires">Forest Fire Dataset</a>, <a href="https://www.srtr.org/">Scientific Registry of Transplant Recipients (SRTR)</a>, <a href="https://www.unsw.adfa.edu.au/unsw-canberra-cyber/cybersecurity/ADFA-IDS-Datasets/">ADFA-LD and IDS</a>, and <a href="https://www.backblaze.com/b2/hard-drive-test-data.html">BackBlaze</a>.
            </p>
            <br/>

            <ul>
                <li><strong>Optimal Exchange and Prediction for Kidney Exchanges.</strong> Every year, thousands of individuals receive kidney transplants that are life saving. Yet, the underlying algorithms for matching chains of donors and predicting various outcomes of kidney transplants are two wide open research fields. This project is focused analyzing real world data to predict various patient outcomes while optimizing kidney transplants. Work will include the use data science techniques, machine learning, and high performance computing. <br/></li>

                <li><strong>Imbalanced Data &amp; Data Augmentation</strong> Imbalanced datasets are common in the real world. I'm interested in augmenting these datasets using Computational Intelligence techniques that blend various types of neural networks with metaheuristic algorithms, particularly Evolutionary GANS.</li>
            </ul>
    </div>
</div>