---
layout: page
title: "Videos"
comments: false
# sharing: false
permalink: /videos/
---

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" style="margin-bottom:10px;">

            <h3>CS 3540 Videos</h3>
            <ul>
                <li><a href="https://www.youtube.com/playlist?list=PLXWA0mX8UFazVuVfxZR04gZ2h8yjkY4nT"><i class="fa fa-youtube"></i></a> Complete Playlist</li>
            </ul>
            <h3>MBA 6010P Videos</h3>
            <ul>
                <li><a href="https://www.youtube.com/watch?v=69qIntHek-M&list=PLXWA0mX8UFazjgf7DUf2pA7mu_ZTjezRC"><i class="fa fa-youtube"></i></a> Complete Playlist</li>
            </ul>   

            <h3>Git</h3>
            <ul>
                <li>
                    <a href="https://www.youtube.com/watch?v=kpsVWIjNmM8"><i class="fa fa-youtube"></i></a> 
                    <a href="https://vimeo.com/357058713"><i class="fa fa-vimeo"></i></a> Git and Gitlab
                </li>
                <li>
                    <a href="https://www.youtube.com/watch?v=aPD3N7HdQRc"><i class="fa fa-youtube"></i></a> 
                    <a href="https://vimeo.com/357059344"><i class="fa fa-vimeo"></i></a> Git and Gitlab: Branch, Merge, and Tag
                </li>
            </ul>

            <h3>Data Science</h3>
            <ul>
                <li>
                    <a href="https://www.youtube.com/watch?v=zg7IEKQIV5w"><i class="fa fa-youtube"></i></a> 
                    <a href="https://vimeo.com/358168223"><i class="fa fa-vimeo"></i></a> Setting up a Basic PyEnv Virtual Environment 
                </li>
                <li>
                    <a href="https://www.youtube.com/watch?v=6gjaJOLqMnQ"><i class="fa fa-youtube"></i></a> 
                    <a href="https://vimeo.com/358167591"><i class="fa fa-vimeo"></i></a> Adding Git to Venv and Cookiecutter 
                </li>
                <li>
                    <a href="https://www.youtube.com/watch?v=8pXOi6y3Cd4"><i class="fa fa-youtube"></i></a> 
                    <a href="https://vimeo.com/358167928"><i class="fa fa-vimeo"></i></a> Using Cookiecutter as a Project Template 
                </li>
            </ul>
            
            <h3>Parallel Computing</h3>
            <ul>
                <li>
                    <a href="https://www.youtube.com/watch?v=gDg3sLq_l_0"><i class="fa fa-youtube"></i></a> 
                    <a href="https://vimeo.com/359637607"><i class="fa fa-vimeo"></i></a> Parallel Computing Data Analysis for 1D Data in Excel 
                </li>
            </ul>
        </div>
    </div>
</div>